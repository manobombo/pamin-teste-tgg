ActiveRecord::Schema.define(version: 20150707190452) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "users", force: true do |t|
  t.string   "sender"                  
  t.string   "receiver"     
  end

  create_table "messages", force: true do |t|
  t.string "message"
  end