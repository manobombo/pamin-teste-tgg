Rails.application.config.middleware.insert_before ::ActionDispatch::Cookies, ::CookieFilter
Rails.backtrace_cleaner.add_silencer { |line| line.start_with? "lib/cookie_filter.rb" }