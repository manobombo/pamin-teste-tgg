(function () {
    'use strict';

    angular
        .module('pamin.main')
        .controller('MainController', MainController);

    MainController.$injector = ['$scope', '$location', 'Notify', 'PaminMap', 'Register', 'Auth'];
    function MainController($scope, $location, Notify, PaminMap, Register, Auth) {
        var vm = this;

        vm.route = $location;
        vm.auth = Auth;
        vm.logout = logout;

        init();

        $scope.$on('refreshMap:main', getRegisters);

        function init() {
            getRegisters();
        }

        function logout() {
            Auth.logout().then( function () {
                Notify.alert('Usuário deslogado com sucesso!');
            });
        }

        function getRegisters() {
            var registers = Register.query(function () {
                angular.forEach(registers, function (register) {
                    var marker =
                        PaminMap.addMarker('register:' + register.id, {
                            position: {
                                lat: parseFloat(register.latitude),
                                lng: parseFloat(register.longitude)
                            },

                            icon: {
                                url: 'static/icons/markers/categories/' + register.category.name + '.svg',
                                size: new google.maps.Size(32, 32),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(16, 32)
                            }
                        }, { // Events
                            click: function(e) {
                                var path = '/registro/' + register.id;
                                $location.path(path == $location.path() ? '/' : path);
                            }
                        });

                        marker.setData({
                            category: register.category.name
                        });

                });

            });
        }
    }
})();