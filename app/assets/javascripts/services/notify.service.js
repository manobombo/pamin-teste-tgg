(function () {
    'use strict';

    angular
        .module('pamin.services')
        .service('Notify', Notify);

    Notify.$injector = ['$mdToast'];

    function Notify($mdToast) {
        var notify = this;

        notify.alert = alert;

        function alert(message) {
            if (message)
                $mdToast.show($mdToast.simple().position('top left right').content(message).hideDelay(6000));
        }
    }
})();