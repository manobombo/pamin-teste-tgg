( function () {
    'use strict';

    angular
        .module('pamin.services')
        .factory('PaminMap', PaminMap);

    PaminMap.$injector = ['$rootScope', 'zigmap'];
    function PaminMap($rootScope, zigmap) {
        var options = {
            center: {
                lat: -7.1292274,
                lng: -34.8460347
            },

            zoom: 14,

            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            },

            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            }
        };

        var events = {
            click: function (event) {
                var coords = {
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng()
                };

                $rootScope.$broadcast('paminMap:click', coords);
            }
        };

        var map = zigmap.addMap('pamin-map', options, events);

        return map;
    }
})();