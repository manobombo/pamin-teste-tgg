// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require angular
//= require angular-aria
//= require angular-animate
//= require hammerjs
//= require angular-material
//= require angular-route
//= require angular-resource
//= require angular-cookies
//= require angular-rails-templates
//= require angular-loading-bar
//= require_self
//= require_tree .

(function () {
    'use strict';

    angular
        .module('pamin', [
            'pamin.core',

            'pamin.main',
            'pamin.services',
            'pamin.categories',
            'pamin.registers',
            'pamin.filters',
            'pamin.users'
        ]);

    angular
        .module('pamin.core', [
            'ngRoute',
            'ngResource',
            'ngMaterial',
            'ngAnimate',
            'templates',

            'pamin.widgets',
            'pamin.sessions',
            'angular-loading-bar',

            'zigMap'
        ]);

    angular.module('pamin.main', []);
    angular.module('pamin.services', []);
    angular.module('pamin.categories', []);
    angular.module('pamin.registers', []);
    angular.module('pamin.users', []);
    angular.module('pamin.widgets', []);
    angular.module('pamin.filters', []);
    angular.module('pamin.sessions', ['ngCookies']);

})();