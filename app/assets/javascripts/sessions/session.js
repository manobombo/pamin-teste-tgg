( function () {
    'use strict';

    angular
        .module('pamin.sessions')
        .service('Session', Session);

    Session.$injector = ['$cookieStore'];

    function Session($cookieStore) {
        var session = this;

        session.setEmail = setEmail;
        session.getEmail = getEmail;
        session.setToken = setToken;
        session.getToken = getToken;
        session.setUser = setUser;
        session.getUser = getUser;

        session.isAuthenticated = isAuthenticated;
        session.destroy = destroy;

        function setEmail(email) {
            $cookieStore.put('email', email);
        }

        function getEmail() {
            return $cookieStore.get('email') || null
        }

        function setToken(token) {
            $cookieStore.put('token', token);
        }

        function getToken() {
            return $cookieStore.get('token') || null
        }

        function isAuthenticated() {
            return getToken() && getEmail();
        }

        function setUser(user) {
            $cookieStore.put('current_user', user);
        }

        function getUser() {
            return $cookieStore.get('current_user');
        }

        function destroy() {
            $cookieStore.remove('token');
            $cookieStore.remove('email');
            $cookieStore.remove('current_user');
        }
    }
})();