( function () {
    'use strict';

    angular
        .module('pamin.widgets')
        .directive('signUpForm', SignUpFormDirective);

    function SignUpFormDirective() {
        return {
            restrict: 'E',
            templateUrl: 'widgets/signup-form.html',
            controller: SignUpFormController
        };
    }

    SignUpFormController.$injector = ['$scope', '$location', 'Notify', 'Auth', 'User'];

    function SignUpFormController($scope, $location, Notify, Auth, User) {
        $scope.user = new User();

        $scope.signup = function () {
            User.save($scope.user, function () {
                    Notify.alert('Cadastrado com sucesso!');
                    var credentials = {
                        email: $scope.user.email,
                        password: $scope.user.password
                    };

                    Auth.login(credentials).then( function () {
                        $location.path('/');
                    });
            }, function (res) {
                console.log(res.data.errors);
                Notify.alert(res.data.errors.join("\n"));
            });
        }
    }
})();