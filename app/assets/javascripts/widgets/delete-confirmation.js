( function () {
    'use strict';

    angular
        .module('pamin.widgets')
        .controller('SimpleDialogController', SimpleDialogController);

    function SimpleDialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
})();