( function () {
    'use strict';

    angular
        .module('pamin.widgets')
        .directive('loginForm', LoginFormDirective);

    function LoginFormDirective() {
        return {
                restrict: 'E',
                templateUrl: 'widgets/login.html',
                link: function (scope) {},
                controller: LoginFormController
            };
    }

    LoginFormController.$injector = ['$scope', 'Notify', '$location', 'Auth'];
    function LoginFormController($scope, Notify, $location, Auth) {
        $scope.credentials = {};

        $scope.login = function () {
            Auth.login($scope.credentials)
                .then( function (user) {
                    Notify.alert('Usuário logado com sucesso!');
                    $location.path('/');
                }, function (errors) {
                    Notify.alert('Email ou senha incorretos.');
                    $scope.credentials.password = '';
                });
        }
    }
})();