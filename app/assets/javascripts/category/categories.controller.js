(function () {
'use strict';

    angular
        .module('pamin.categories')
        .controller('CategoriesController', CategoriesController);

    // Dependencies
    CategoriesController.$injector = ['Category', 'PaminMap', '$filter'];

    function CategoriesController(Category, PaminMap, $filter) {
        var vm = this;
        // Getting the all categories
        vm.categories = Category.query();

        vm.filter = filter;
        vm.isActive = isActive;

        function filter(category) {
            if(!isActive(category)) {
                $filter('byCategory')(PaminMap.getMarkers(), category.name);
                categoryActive = category.id;
            } else {
                removeFilter();
                categoryActive = null;
            }
        }


        var categoryActive = null;
        function isActive(category) {
            return categoryActive === category.id
        }

        function removeFilter() {
            angular.forEach(PaminMap.getMarkers(), function (marker) {
                marker.getReference().setVisible(true);
            });
        }
    }

})();