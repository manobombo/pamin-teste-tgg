( function () {
    'use strict';

    angular
        .module('pamin.registers')
        .factory('Register', Register);

    Register.$injector = ['$resource'];

    function Register($resource) {
        return $resource('/api/registers/:id', {
            update: {
                method: 'PUT'
            }
        });
    }
})();