(function () {
    'use strict';

    angular
        .module('pamin.categories')
        .factory('Category', Category);

    Category.$injector = ['$resource'];
    function Category($resource) {
        return $resource('/api/categories');
    }

})();