( function () {
    'use strict';

    angular
        .module('pamin')
        .config(Config);

    Config.$injector = ['$locationProvider', '$routeProvider', '$httpProvider', 'cfpLoadingBarProvider'];

    function Config($locationProvider, $routeProvider, $httpProvider, cfpLoadingBarProvider) {
        $locationProvider.html5Mode(true);
	$routeProvider
            .when('/categorias', {
                templateUrl: 'categories.html',
                controller: 'CategoriesController',
                controllerAs: 'vm'
            })

            .when('/registro/novo', {
                templateUrl: 'registers/new-register.html',
                controller: 'NewRegisterController',
                controllerAs: 'vm',
                resolve: {
                    auth: AccessRestrict
                }
            })
            .when('/registro/:id', {
                templateUrl: 'registers/details-register.html',
                controller: 'DetailsRegisterController',
                controllerAs: 'vm'
            })

            .when('/sobre', {
                templateUrl: 'about.html'
            })

            .when('/entrar', {
                templateUrl: 'login.html'
            })

            .when('/cadastro', {
                templateUrl: 'signup.html'
            })

            .otherwise({
                redirectTo: '/'
            });


        $httpProvider.interceptors.push('SessionInjector');

        cfpLoadingBarProvider.includeSpinner = false;
    }

    function AccessRestrict($location, $q, Auth) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        Auth.validateToken().then( function () {
            deferred.resolve();
        },function () {
            $location.path('/entrar');
            deferred.reject();
        });

        return promise;
    }
})();
