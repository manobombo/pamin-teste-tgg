( function () {
    'use strict';

    angular
        .module('pamin.registers')
        .controller('DetailsRegisterController', DetailsRegisterController);

    DetailsRegisterController.$injector = [
        'Register',
        '$routeParams',
        '$mdDialog',
        'Notify',
        '$window',
        '$location',
        '$mdDialog',
        'PaminMap',
        'Auth'];
    function DetailsRegisterController(Register, $routeParams, Notify, $window, $location, $mdDialog, PaminMap, Auth) {
        var vm = this;

        // Get register
        vm.register = Register.get({id: $routeParams.id}, function () {
            moveMarker();
        });

        vm.destroy = destroy;
        vm.isAuthorized = isAuthorized;

        // Animation to let visible the marker
        function moveMarker() {
            var center = {
                lat: parseFloat(vm.register.latitude),
                lng: parseFloat(vm.register.longitude)
            }

            PaminMap.getReference().setCenter(center);
            PaminMap.getReference().panBy(-($window.innerWidth / 4), 0);
        }

        // Remove the register
        function destroy(ev) {
            // Confirmation Dialog
            $mdDialog.show({
                templateUrl: 'widgets/delete-confirmation.html',
                controller: 'SimpleDialogController',
                targetEvent: ev
            }).then(function() {
                Register.delete(vm.register, function () {
                    Notify.alert('Registro excluido!');
                    PaminMap.removeMarker('register:' + $routeParams.id);
                    $location.path('/');
                });
            });
        }

        function isAuthorized() {
            if ( Auth.current_user() && vm.register.user )
                return Auth.isAuthenticated() && Auth.current_user().id === vm.register.user.id;
        }
    }
})();