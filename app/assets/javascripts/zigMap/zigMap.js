(function () {
    'use strict';

    angular.module('zigMap', []);
})();

(function () {
    'use strict';

    angular
        .module('zigMap')
        .factory('zigmap', zigmap);

    zigmap.$injector = ['$rootScope'];
    function zigmap($rootScope) {
        var factory = {};

        factory.maps = {};

        factory.getMap = getMap;
        factory.addMap = addMap;

        function addMap( idKey, options, events) {
            factory.maps[idKey] = new Map(idKey, options, events);
            return factory.maps[idKey];
        }

        function getMap(idKey) {
            return factory.maps[idKey];
        }

        function Marker(idKey, options, events) {
            this.idKey = idKey;
            this.options = options;
            this.events = events;

            this.gmarker = new google.maps.Marker(options);
            this.setEvents(events);
        }

        Marker.prototype = {
            getReference: function () {
                return this.gmarker;
            },
            setEvents: function (events) {
                var self = this;
                // Binding the events
                angular.forEach(self.events, function (eventFn, event) {
                    var fn = function (event) {
                        eventFn(event);
                        $rootScope.$apply();
                    };

                    google.maps.event.addListener(self.gmarker, event, fn);
                });
            },
            getOptions: function () {
                return this.options;
            },
            setData: function (data) {
                this.data = data;
            },
            getData: function () {
                return this.data;
            }
        };

        function Map(idKey, options, events) {
            this.idKey = idKey;
            this.options = options;
            this.events = events;

            this.gmap = null;

            this.markers = {};
        }

        Map.prototype = {
            setReference: function (canvas) {
                var self = this;
                self.gmap = new google.maps.Map(canvas, self.options);

                // Binding the markers
                angular.forEach(self.markers, function (marker) {
                   marker.getReference().setMap(self.gmap);
                });

                this.setEvents(this.events);
            },
            getReference: function () {
                return this.gmap;
            },
            setEvents: function (events) {
                var self = this;
                // Binding the events
                angular.forEach(self.events, function (eventFn, event) {
                    var fn = function (event) {
                        eventFn(event);
                        $rootScope.$apply();
                    };

                    google.maps.event.addListener(self.gmap, event, fn);
                });
            },
            addMarker: function (idKey, options, events) {
                options = angular.extend({
                    map: this.gmap
                }, options);

                var marker = this.markers[idKey];

                if ( marker === undefined ) {
                    marker = new Marker(idKey, options, events);
                    this.markers[idKey] = marker;
                } else {
                    marker.getReference().setOptions(options);
                }

                return marker;
            },
            removeMarker: function (idKey) {
                var marker = this.markers[idKey];

                try {
                    marker.getReference().setMap(null);
                    delete this.markers[idKey];
                }catch( err ) {
                    console.error('Error: Marker undefined');
                }
            },
            getMarkers: function () {
                return this.markers;
            },
            setDefaultZoom: function () {
                this.gmap.setZoom(this.options.zoom);
            }
        };

        return factory;
    }

})();

( function () {
    'use strict';

    angular
        .module('zigMap')
        .directive('zigMap', zigMapDirective);

    zigMapDirective.$injector = ['zigmap'];
    function zigMapDirective(zigmap) {
        var directive = {
            restrict: 'E',
            template: '<div></div>',
            scope: {
                id: '@'
            },
            link: link
        };

        return directive;

        function link($scope, $elem) {
            $elem.children().addClass('zig-map');
            var map = zigmap.getMap($scope.id);
            try {
                map.setReference($elem.children()[0]);
            } catch( err ) {
                console.debug(err);
                console.error('The map called ' + $scope.id + ' don\'t exists. Please, create a map containing the id \'' + $scope.id + '\' and add it to zigMap service.');
            }
        }
    }
})();