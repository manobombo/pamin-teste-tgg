class Api::SessionsController < Devise::SessionsController

  skip_before_filter :verify_signed_out_user
  respond_to :json

  def create
    self.resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
    sign_in(resource_name, resource)
    current_api_user.update authentication_token: nil

    render status: :ok,
                json: {
                    user: current_api_user,
                    authentication_headers: {
                        user_email: current_api_user.email,
                        user_token: current_api_user.authentication_token
                    }
                }
  end

  def destroy
    if current_api_user
      current_api_user.update authentication_token: nil
      sign_out
      render nothing: true, status: :ok
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  def failure
    render nothing: true, status: 401
  end

  def validate_token
    if current_api_user
      render nothing: true, status: :ok
    else
      render nothing: true, status: 401
    end
  end

end