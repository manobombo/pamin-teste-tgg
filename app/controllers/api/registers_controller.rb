class Api::RegistersController < ApplicationController

  before_action :authenticate_api_user!, only: [:create,:update,:destroy]
  after_action :verify_authorized, :only => [:update,:destroy]

  def index
    @registers = Register.all
  end

  def show
    @register = Register.find(params[:id])
  end

  def create
    @register = current_api_user.registers.build(register_params)
    if @register.save
      render nothing: true, status: :created
    else
      render json: '{errors: @register.erros}', status: :unprocessable_entity
    end
  end

  def update

    @register = Register.find(params[:id])
    authorize @register
    if @register.update(register_params)
      render nothing: true, status: :ok
    else
      render json: '{errors: @register.erros}', status: :unprocessable_entity
    end
  end

  def destroy
    @register = Register.find(params[:id])
    authorize @register
    @register.destroy

    render nothing: true, status: :ok
  end

  private

    def register_params
      params.require(:register).permit(:what, :where, :start_date, :end_date, :price, :promotor, :promotor_contact, :description, :latitude, :longitude, :category_id)
    end

end
