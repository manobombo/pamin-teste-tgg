class Api::RegistrationsController < Devise::RegistrationsController


  def create
    build_resource(sign_up_params)
    if resource.save
      render status: :ok,
                 json:  { user: resource }
    else
      render status: :unprocessable_entity,
             :json => { errors: resource.errors.full_messages }
    end
  end


  def sign_up_params
    # Correct: params.require(:user).permit(:name,:email,:birthday, :password, :password_confirmation)
    # Gambis
    params.permit(:name,:email,#:birthday,
     :password, :password_confirmation)
  end

end