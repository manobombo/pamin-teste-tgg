class RegisterPolicy
  attr_reader :user, :register

  def initialize(user, register)
   @user = user
   @register = register
  end

  def update?
    @register.user == user
  end

  def destroy?
    @register.user == user
  end

end